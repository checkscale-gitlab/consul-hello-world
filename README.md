# Consul Hello World

Playground around [consul](https://www.consul.io) and [python-consul](https://python-consul.readthedocs.io/en/latest/).

- Setup a consul cluster with docker
- Service registration
- Leadership acquisition

## Bring up consul cluster

```
docker-compose up
```

After a short while (cluster is sync)...
```
$ docker exec server consul members
Node          Address          Status  Type    Build  Protocol  DC   Segment
081fc72835fe  172.28.0.3:8301  alive   server  1.6.1  2         dc1  <all>
ac5bebfa0635  172.28.0.6:8301  alive   server  1.6.1  2         dc1  <all>
e3574ac5149d  172.28.0.2:8301  alive   server  1.6.1  2         dc1  <all>
6b8a39079ac6  172.28.0.4:8301  alive   client  1.6.1  2         dc1  <default>
6fc094b45934  172.28.0.7:8301  alive   client  1.6.1  2         dc1  <default>
72137da9c4aa  172.28.0.5:8301  alive   client  1.6.1  2         dc1  <default>
```

Or via curl
```
curl http://127.0.0.1:8500/v1/agent/members
```

Or via the consul web ui at
http://localhost:8500/ui/dc1/services

## Leader election

Check leader/
