""" Main service, replicated, with a master. """
import asyncio
import argparse
from contextlib import suppress
from functools import partial
import logging
import signal

from aiohttp import web
import consul


class ConsulInterface:
    """ Interact with consul to get register the instance role. """

    def __init__(self):
        self._consul = consul.Consul()
        self._session_id = None
        self.logger = logging.getLogger("consul interface")
        self.is_leader = False

    def register(self, address=None, port=None):
        """ Register the instance for this service from consul.
        Create the service if not exist.
        """
        self.logger.info("register to service app...")
        self._consul.agent.service.register("app", address=address, port=port)

    def create_session(self):
        """ Get a session ID for the service. """
        self.logger.info("create session...")
        self._session_id = self._consul.session.create("app", lock_delay=600)
        self.logger.info("session id: %s", self._session_id)

    def acquire_session(self):
        """ Try to acquire a session.

        :returns: True if the session is acquired, i.e. instance is the leader
        """
        self.logger.info("acquire session...")
        self.is_leader = self._consul.kv.put(key="app", value=None,
                                             acquire=self._session_id)
        self.logger.info("leader: %s", self.is_leader)

    def release_session(self):
        """ Release the leader role. """
        self.logger.info("release session...")
        success = self._consul.kv.put(key="app", value=None,
                                      release=self._session_id)
        self.is_leader = self.is_leader and not success
        self.logger.info("leader: %s", self.is_leader)


class App:
    """ Sample http app that tell use if it's the leader. """

    def __init__(self, ci):
        """
        :param ConsulInterface ci:
        """
        self.ci = ci
        # setup http server
        aiohttp_app = web.Application()
        aiohttp_app.add_routes([
            web.get('/', self.root),
            web.get('/pdb', self.pdb)
        ])
        self.runner = web.AppRunner(aiohttp_app)
        self._close_event = asyncio.Event()
        self._worker_task = asyncio.ensure_future(self._worker())

    @classmethod
    async def create(cls, host, port):
        """ Create a new App instance, starting the web server. """
        # setup consul
        ci = ConsulInterface()
        app = cls(ci)
        try:
            await app.start(host, port)
        except Exception as exc:
            raise exc
        else:
            ci.register(host, port)
            ci.create_session()
            ci.acquire_session()
        return app

    async def start(self, host, port):
        """ Start http server. """
        await self.runner.setup()
        site = web.TCPSite(self.runner, host, port)
        await site.start()
        logging.info("App ready on %s:%s", host, port)

    def close(self):
        """ Gracefull shutdown. """
        logging.info("Closing app...")
        self._close_event.set()

    async def wait_closed(self):
        """ Wait until app is terminated. """
        await self._close_event.wait()
        # clean
        self._worker_task.cancel()
        with suppress(asyncio.CancelledError):
            await self._worker_task

        self.ci.release_session()

        await self.runner.cleanup()

    async def root(self, request):
        """ Simple request handler. """
        return web.Response(text=f"I'm leader is {self.ci.is_leader}")

    async def pdb(self, request):
        """ Trigger a pdb. """
        import pdb
        pdb.set_trace()
        return web.Response(text="OK")

    async def _worker(self):
        """ Loop to periodically request the leadership from consul. """
        while True:
            try:
                self.ci.acquire_session()
            except:
                logging.exception("Error requesting leadership.")
            else:
                logging.info("I'm leader: %s", self.ci.is_leader)
            await asyncio.sleep(5)


def main():
    # Get args
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",
                        help="Host, default localhost.",
                        default="localhost")
    parser.add_argument("-p", "--port",
                        help="Port to bind, default 80.",
                        default=80,
                        type=int)
    parser.add_argument("-v", "--verbosity",
                        help="Log level DEBUG|INFO|WARNING...",
                        default="INFO")
    args = parser.parse_args()
    # Configure logger
    logging.basicConfig(level=args.verbosity.upper())

    # start the App
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(App.create(args.host, args.port))

    # Register signal handlers
    def shutdown(sig):
        logging.info("received signal %s, shutting down", sig)
        app.close()

    loop.add_signal_handler(signal.SIGINT, partial(shutdown, "SIGINT"))
    loop.add_signal_handler(signal.SIGTERM, partial(shutdown, "SIGTERM"))

    # Run service until closed
    loop.run_until_complete(app.wait_closed())
    loop.remove_signal_handler(signal.SIGINT)
    loop.remove_signal_handler(signal.SIGTERM)
    logging.info("terminate.")


if __name__ == '__main__':
    main()
