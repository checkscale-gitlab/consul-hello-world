""" Sample consul interaction with the Key-Value store. """
import consul

c = consul.Consul()
c.kv.put('foo', 'bar')

index, data = c.kv.get('foo')
print(index, data['Value'])

# this will block until there's an update or a timeout
index, data = c.kv.get('foo', index=index)
